<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: X-Requested-With");

require_once('config.php');

$idp = $_GET["idp"];

try {
    $db = new PDO("mysql:host=" . HOSTNAME . ";dbname=" . DBNAME, USER, PASSWORD);
} catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}


$query = "SELECT * FROM playlist WHERE id=:idp";
$stmt = $db->prepare($query);
$stmt->execute(array(":idp"=>$idp));
$playlist = $stmt->fetch();

if($playlist) {
    $query = "SELECT * FROM specifiche_playlist WHERE idplaylist=:idp";
    $stmt = $db->prepare($query);
    $stmt->execute(array(":idp"=>$idp));
    $playlist["specifiche"] = $stmt->fetchAll();

    $query = "SELECT * FROM filmati WHERE idplaylist=:idp ORDER BY titolo";
    $stmt = $db->prepare($query);
    $stmt->execute(array(":idp"=>$idp));
    $playlist["filmati"] = $stmt->fetchAll();
}

echo json_encode($playlist);
?>