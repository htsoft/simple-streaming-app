<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: X-Requested-With");

require_once('config.php');

try {
    $db = new PDO("mysql:host=" . HOSTNAME . ";dbname=" . DBNAME, USER, PASSWORD);
} catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}


$query = "SELECT id, nome, genere, foto FROM playlist ORDER BY nome";
$stmt = $db->prepare($query);
$stmt->execute();
$elenco = $stmt->fetchAll();

echo json_encode($elenco);
?>