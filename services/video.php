<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: X-Requested-With");

require_once('config.php');

try {
    $db = new PDO("mysql:host=" . HOSTNAME . ";dbname=" . DBNAME, USER, PASSWORD);
} catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

$idfilmato = $_GET["idf"];
$query = "SELECT id, link FROM filmati where id=:idf";
$stmt = $db->prepare($query);
$stmt->execute(array(":idf"=>$idfilmato));
$filmato = $stmt->fetch();


// Resituisce il file CSV come attachment
$filename = "../filmati/" . $filmato["link"];
if(file_exists($filename)) {
    http_response_code(200);
    header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
    header("Cache-Control: public");
    header("Content-Type: video/mp4");
    header("Content-Transfer-Encoding: Binary");
    header("Content-Length:".filesize($filename));
    //header("Content-Disposition: attachment; filename=file.zip");
    readfile($filename);
    die();            
} else {
    echo "Il file: " . $filename . " non è stato trovato.";
}
