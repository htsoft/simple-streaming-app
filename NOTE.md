# APPUNTI SPARSI 

Pagina Facebook di riferimento: https://www.facebook.com/yumegears
Pagina del progetto su GitLab : https://gitlab.com/htsoft/simple-streaming-app

# COSA E' NECESSARIO

- PHP
- SPAZIO WEB (LINUX)
- MYSQL
- VUE E QUASAR
- ACCESSO FTP

# SERVIZI

- L'elenco delle playlist disponibili
- L'elenco dei filmati disponibili
- Lo stream di un file
