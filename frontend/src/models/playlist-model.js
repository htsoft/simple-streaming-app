export interface PlaylistElement {
  id: number;
  nome: string;
  genere: string;
  foto: string;
}
